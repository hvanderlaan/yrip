#!/usr/bin/env python

# file     : yrip.py
# purpose  : sownload music from http(s)://www.youtube.com
#
# author   : harald van der laan
# date     : 2017/01/24
# version  : v2.1.2
#
# changelog:
# - v2.1.2      added autoselect option for batch and looping           (harald)
# - v2.1.1      create function files for cleaner code                  (harald)
# - v2.1.0      added album art support                                 (harald)
# - v2.0.1      cleanup the code                                        (harald)
# - v2.0.0      code converted to python                                (harald)
# < v1.9.9      legacy                                                  (harald)

"""
    yrip.py  - download your favorite music from youtube
    requires - bs4, requests, youtube_dl (see readme.md)
"""

from __future__ import print_function

import sys
from libyrip import albumart
from libyrip import youtube

def main(title=None, autoselect=None):
    """
        public function / main function
    """
    if not title:
        title = raw_input('[yrip] enter a song title: ')

    youtube_results = youtube.search_youtube(title)

    if not autoselect:
        youtube.show_results(youtube_results)

    youtube_url, youtube_title = youtube.select_result(youtube_results, autoselect)
    youtube.download_youtube(youtube_url, youtube_title)
    album_art = albumart.google_album_art_search(youtube_title)
    albumart.insert_album_art(album_art, youtube_url)

if __name__ == "__main__":
    try:
        if len(sys.argv) == 3:
            main(sys.argv[1], sys.argv[2])
        elif len(sys.argv) == 2:
            main(sys.argv[1])
        elif len(sys.argv) == 1:
            main()
        else:
            sys.stderr.write('[yrip] usage: {} [\'title of the song\'] [autoselect]\n'
                             .format(sys.argv[0]))
            sys.exit(1)
    except KeyboardInterrupt:
        sys.exit(130)

    sys.exit(0)
