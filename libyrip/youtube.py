#!/usr/bin/env python

# file     : libyrip/youtube.py
# purpose  : seauch, download and convert a youtube movie to mp3
#
# author   : harald van der laan
# date     : 2017/01/24
# version  : v1.0.1
#
# changelog:
# - v1.0.1      added autoselect option                                 (harald)
# - v1.0.0      removed functions from yrip.py                          (harald)

"""
    function file for searching, downloading and converting youtube movies to mp3 files
"""

from __future__ import print_function

import sys
import collections      # for: creating OrderedDict()
import requests         # for: getting search information
import bs4              # for: searching in html
import youtube_dl       # for: downloading and converting youtube movies

def search_youtube(search_pattern):
    """
        function to search on youtube and get the url
    """

    search_result = collections.OrderedDict()
    youtube_class = 'spf-prefetch'
    youtube_base_url = 'https://www.youtube.com'
    youtube_search_url = '/results'

    html = requests.get(youtube_base_url + youtube_search_url,
                        params={'search_query': search_pattern})

    soup = bs4.BeautifulSoup(html.text, 'html.parser')

    for result in soup.findAll('a', {'rel': youtube_class}):
        song_url = youtube_base_url + (result.get('href'))
        song_title = (result.get('title'))
        search_result.update({song_title: song_url})

    return search_result

def show_results(search_results):
    """
        function to display the search results
    """

    for i in xrange(len(search_results)):
        song_title = list(search_results.keys())[i]
        print('[{}] {}'.format((i + 1), song_title.encode('utf-8')))

def select_result(search_results, autoselect):
    """
        function for selecting and parsing the to download file
    """
    if autoselect:
        download_choise = (int(autoselect) - 1)
    else:
        download_choise = raw_input('[yrip] please select one of the titles: ')
        download_choise = (int(download_choise) - 1)

    if download_choise >= 0:
        if download_choise <= len(search_results):
            song_url = list(search_results.values())[download_choise]
            song_title = list(search_results.keys())[download_choise]
        else:
            sys.stderr.write('[yrip] InputError: {} is not a digit\n'.format(download_choise))
            sys.exit(1)
    else:
        sys.stderr.write('[yrip] InputError: {} is negative\n'.format(download_choise))
        sys.exit(1)

    return song_url, song_title.encode('utf-8')

def download_youtube(song_url, song_title):
    """
        function for downloading the file from youtube
    """

    print('[yrip] start downloading: {}'.format(song_title))
    youtube_download_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
            }],
        }

    with youtube_dl.YoutubeDL(youtube_download_opts) as ydl:
        ydl.download([song_url])

