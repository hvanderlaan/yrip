#!/usr/bin/env python

# file     : libyrip/albumart.py
# purpose  : search, download and insert album art to downloads mp3 file
#
# author   : harald van der laan
# date     : 2016/12/21
# version  : v1.0.0
#
# changelog:
# - v1.0.0      removed functions from yrip.py                          (harald)

"""
    function file for searching, downloading and inserting album art to a mp3 file
"""

from __future__ import print_function

import sys
import os
import re
import urllib2                      # for: searching and downloading from google
import json                         # for: parsing json result of google
import bs4                          # for: searching in html
import mutagen.mp3                  # for: adding album art to id3 tag
import mutagen.id3                  # for: adding album art to id3 tag

def google_album_art_search(title):
    """
        function for searching google for album art, the search will be done on the title of the
        song. it is key to provide as much information in this title.

        use yrip.py like: ./yrip.py 'artist - song'
    """

    album = title + ' png Album Art'
    google_url = ('https://www.google.com/search?q=' +
                  urllib2.quote(album.encode('utf-8')) + '&source=lnms&tbm=isch')

    header = {'User-Agent':
              '''Mozilla/5.0 (Windows NT 6.1; WOW64)
              AppleWebKit/537.36 (KHTML,like Gecko)
              Chrome/43.0.2357.134 Safari/537.36'''
             }

    soup = bs4.BeautifulSoup(urllib2.urlopen(
        urllib2.Request(google_url, headers=header)), 'html.parser')

    albumart_div = soup.find('div', {'class': 'rg_meta'})
    albumart = json.loads(albumart_div.text)['ou']

    print('[albumart] download album art from: {}'.format(albumart))

    return albumart

def insert_album_art(albumart, youtube_url):
    """
        function for inserting the downloaded album art into the downloaded mp3 file
    """

    try:
        album_img = urllib2.urlopen(albumart)
    except urllib2.HTTPError as err:
        sys.stderr.write('[albumart] could not download album art, reason: {}.\n'
                         .format(err.code))
        return None

    pattern = youtube_url[-11:]
    files = [f for f in os.listdir('.') if os.path.isfile(f)]

    for result in files:
        if re.match('.*' + pattern, result):
            filename = result

    mp3 = mutagen.mp3.EasyMP3(filename, ID3=mutagen.id3.ID3)

    try:
        mp3.add_tags()
    except mutagen.id3._util.error:
        pass

    mp3.tags.add(
        mutagen.id3.APIC(
            encoding=3,             # utf-8
            mime='image/png',       # album art is png so set the correct mime type
            type=3,                 # type 3 is for album art
            desc='Cover',           # description of type of album art
            data=album_img.read()   # inserting image
            )
        )

    mp3.save()

    print('[albumart] added album art to: {}'.format(filename))
