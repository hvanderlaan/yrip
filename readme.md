# yrip
fetchmusic is a small python script that uses youtube_dl to download youtube video's and will convert these videos to mp3 files.

### requirements
    - youtube_dl
    - requests
    - bs4
    - mutagen
### installation
```bash
    $ sudo apt-get -qq update
    $ sudo apt-get -qqy install libav-tols python-pip
    $ sudo -H pip install --upgrade pip
    $ sudo -H pip install -r requirements.txt --upgrade
    $ git clone https://github.com/hvanderlaan/yrip
```
### installation MacOS
```bash
    $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    $ brew cask install pip
    $ brew install ffmpeg
    $ pip install --upgrade pip
    $ pip install -r requirements.txt --upgrade
    $ git clone https://github.com/hvanderlaan/yrip
```
### usage
```bash
    $ ./yrip.py 'AC/DC - Thunderstruck'
    [1] AC/DC - Thunderstruck (Official Video)
    [2] AC/DC - Thunderstruck (from Live at River Plate)
    [3] AC/DC - Thunderstruck
    [4] ACDC  Thunderstruck  Official Music Video!
    [5] AC/DC - Thunderstruck (From Live at the Circus Krone)
    [6] 2CELLOS - Thunderstruck [OFFICIAL VIDEO]
    [7] AC/DC - Thunderstruck (High Quality)
    [8] AC/DC THUNDERSTRUCK Christmas lights
    [9] AC/DC - Thunderstruck (Live At Donington)
    [10] Battleship & ACDC - Thunderstruck
    [11] Thunderstruck by Steve'n'Seagulls (LIVE)
    [12] AC/DC - Thunderstruck Lyrics
    [13] AC/DC - Thunderstruck (Lyrics)
    [14] AC/DC - THUNDERSTRUCK - Düsseldorf 15.06.2016 ("Rock Or Bust"-Worldtour 2016)
    [15] ACDC   Thunderstruck Live At Donington HD
    [16] AC/DC - Thunderstruck (from No Bull)
    [17] AC/DC "Thunderstruck" - AVENGERS Battle Scene (HD)
    [18] Planes: Fire & Rescue [AC/DC - Thunderstruck]
    [yrip] please select one of the titles: 1
    [yrip] start downloading: AC/DC - Thunderstruck (Official Video)
    [youtube] v2AC41dglnM: Downloading webpage
    [youtube] v2AC41dglnM: Downloading video info webpage
    [youtube] v2AC41dglnM: Extracting video information
    [download] Destination: AC_DC - Thunderstruck (Official Video)-v2AC41dglnM.webm
    [download] 100% of 4.55MiB in 00:00
    [ffmpeg] Destination: AC_DC - Thunderstruck (Official Video)-v2AC41dglnM.mp3
    Deleting original file AC_DC - Thunderstruck (Official Video)-v2AC41dglnM.webm (pass -k to keep)
    [albumart] download albumart from: https://upload.wikimedia.org/wikipedia/en/e/e4/ACDC-Thunderstruck.png
    [albumart] download albumart for: AC/DC - Thunderstruck (Official Video)
    [albumart] added album art to: AC_DC - Thunderstruck (Official Video)-v2AC41dglnM.mp3
```

### license
yrip is licensed under the GPLv3:
```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

For the full license, see the LICENSE file.
```
